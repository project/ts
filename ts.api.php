<?php
/*
 * The generic functions that can be used developers to integrate with this
 * module.
 */

 /**
  * A helper function to be used by other modules to make Drupal aware of
  * templates files inside other modules' template directories.
  * Usually used inside hook_theme_registry_alter to minimize the code
  * $theme_registry: The entire cache of theme registry information,
  * post-processing.
  * $module: the machine name of the used module.
  * $tpl_path: the path where the templates are located, starting from the module
  * root.
  */
function ts_load_template(&$theme_registry, $module, $tpl_path) {
  // Defined path to the current module.
  $module_path = drupal_get_path('module', $module);
  // Find all .tpl.php files in this module's folder recursively.
  $template_file_objects = drupal_find_theme_templates($theme_registry, '.tpl.php', $module_path . $tpl_path);
  // Iterate through all found template file objects.
  foreach ($template_file_objects as $key => $template_file_object) {
    // If the template has not already been overridden by a theme.
    if (!isset($theme_registry[$key]['theme path']) || !preg_match('#/themes/#', $theme_registry[$key]['theme path'])) {
    // Alter the theme path and template elements.
    $theme_registry[$key]['theme path'] = $module_path;
    $theme_registry[$key] = array_merge($theme_registry[$key], $template_file_object);
    $theme_registry[$key]['type'] = 'module';
    }
  }
}

/*
 * Helper function to load CSS and JS files in a specific location if exsist.
 * $field_name_raw: is the raw field name, keep
 */
function ts_load_css_js($field_name_raw, $module = 'ts', $css_path = 'css', $js_path = 'js') {
  if (file_exists(drupal_get_path('module', $module) . '/' . $css_path . '/' . $field_name_raw . '.css')) {
    drupal_add_css(drupal_get_path('module', $module) . '/' . $css_path . '/' . $field_name_raw . '.css');
  }
  // Include the css file if exist.
  if (file_exists(drupal_get_path('module', $module) . '/' . $js_path . '/' . $field_name_raw . '.js')) {
    drupal_add_js(drupal_get_path('module', $module) . '/' . $js_path . '/' . $field_name_raw . '.js');
  }
}

/**
 * implments hook_field_settings().
 */

function ts_load_entities() {
 // Options to be used to build the lsit of entities and bundles.
 $options = array();
 foreach (entity_get_info() as $entity_type => $entity_info) {
   if (empty($entity_info['fieldable'])) {
     continue;
   }
   foreach($entity_info['bundles'] as $bundle_name => $bundle) {
     // Prefix the bundle name with the entity type.
     $entity_name = check_plain("$entity_info[label] ($entity_type)");
     $options[$entity_name][$entity_type . ':' . $bundle_name] = filter_xss($bundle['label']);
   }
 }
 $form = array(
   '#title' => t('Bundles'),
   '#type' => 'select',
   '#options' => $options,
 );
 return $form;
}

/**
 * Helper function to define the control voabularies.
 */
 function ts_control_vocabs($website_sections = FALSE) {
   $control_vocabs = array(
     'node_template' => 'Entity Template',
     'nodes_term_template' => 'Contents in section Template',
   );
   // include website_section vocabulary if true as it is not related to the
   // control of the templates directly.
   if ($website_sections = TRUE) {
     $control_vocabs['website_sections'] = 'Website Sections';
   }
   return $control_vocabs;
 }

 /**
  * Helper function to define the control fields.
  */
function ts_control_fields($website_sections = FALSE) {
  // getting a list of the control vocabularies.
  $control_fields = ts_control_vocabs($website_sections);
  foreach ($control_fields as $key => $value) {
    // Defining the fields to be used during the fields creation.
    $control_fields['field_' . $key] = $value;
    // removing the original keys.
    unset($control_fields[$key]);
  }
  return $control_fields;
}
