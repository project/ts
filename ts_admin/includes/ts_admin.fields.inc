<?php

/**
 * implments hook_field_settings().
 */
function ts_admin_field_settings() {
  $entity_info = entity_get_info();
  $form = array();
  $options = array();

  // Field set to organize the interface.
  $form['link_fields_entities'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add new relation'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Options to be used to build the lsit of entities and bundles.
  $form['link_fields_entities']['bundle'] = ts_load_entities();

  $form['link_fields_entities']['template_control'] = array(
    '#type' => 'select',
    '#title' => t('Control Field'),
    '#options' => ts_control_vocabs($website_sections = TRUE),
    '#description' => t('Select the template control field that you want to use with the bundle.'),
  );

  // Submit button.
  $form['link_fields_entities']['actions'] = array('#type' => 'actions');
  $form['link_fields_entities']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add field'),
  );

  // Get the fields that exist in the bundle.
  $field_enabled = array();
  $control_vocabs = ts_control_fields($website_sections = TRUE);

  $control_vocabs_name = array_keys($control_vocabs);


  foreach (field_info_field_map() as $field_name => $field) {
    if (in_array($field_name, $control_vocabs_name) && !empty($field['bundles'])) {
      foreach ($field['bundles'] as $entity_type => $bundles) {
        foreach ($bundles as $bundle) {
          $field_enabled[$entity_type][$bundle][] = $field_name;
        }
      }
    }
  }

  if ($field_enabled) {
    $form['control_fields'] = array(
      '#type' => 'vertical_tabs',
      '#weight' => 99,
    );

    // Show all the control fields of each bundle.
    foreach ($field_enabled as $entity_type => $bundles) {
      foreach ($bundles as $bundle => $fields) {
        $options = array();
        $bundles = field_info_bundles($entity_type);
        $form['control_fields_' . $entity_type . '_' . $bundle] = array(
          '#type' => 'fieldset',
          '#title' => t('@bundle - @entity entity', array('@bundle' => $bundles[$bundle]['label'], '@entity' => $entity_info[$entity_type]['label'])),
          '#collapsible' => TRUE,
          '#group' => 'control_fields',
        );

         foreach ($fields as $field_name) {
           $field = array();
           $field = field_info_instance($entity_type, $field_name, $bundle);
          $options[] = array(
            check_plain($field['label']),
            filter_xss($field['description']),
            l(t('Delete'), "admin/config/user-interface/ts/fields/$entity_type/$bundle/$field_name/delete"),
          );
        }

        $header = array(t('Field'), t('Description'), t('Operations'));
        $form['control_fields_' . $entity_type . '_' . $bundle]['fields'] = array(
          '#markup' => theme('table', array('header' => $header, 'rows' => $options)),
        );

      }
    }
  }
  else {
    $form['control_fields'] = array(
      '#markup' => t('There are no template control fields attached to any bundle yet.'),
    );
  }

  return $form;
}

/**
 * Submit handler; Attach field can to bundle.
 */
function ts_admin_field_settings_submit($form, &$form_state) {
  list($entity_type, $bundle) = explode(':', $form_state['values']['bundle']);
  $vocab = $form_state['values']['template_control'];
  $field_name = 'field_' . $vocab;
  $label = ucwords(str_replace('_', ' ', str_replace('field_', '', $field_name)));
  $field = array(
    'field_name' => $field_name,
    'type' => 'taxonomy_term_reference',
    'settings' => array(
      'allowed_values' => array(
        array(
          'vocabulary' => $vocab,
          'parent' => 0
        ),
      ),
    ),
  );
  // Check if field exists.
  if (field_info_field($field['field_name']) === NULL) {
    // If not, create it.
    field_create_field($field);
    drupal_set_message(var_dump($vocab = $form_state) . t('successful added field <b>' . $field['field_name'] . '.</b>'), 'status', FALSE);
  }


  if (empty(field_info_instance($entity_type, $field_name, $bundle))) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'label' => $label,
      'description' => 'Control field that controles ' . $label . '.',
      'bundle' => $bundle,
      'required' => FALSE,
      'widget' => array(
        'type' => 'options_select'
        ),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'taxonomy_term_reference_plain',
          'weight' => '11',
          'settings' => array(),
          'module' => 'taxonomy',
        ),
      ),
    );

    field_create_instance($instance);
    drupal_set_message(t('successful added field <b>' . $field_name . '</b> to the entity <b>' . $bundle . '</b>.'), 'status', FALSE);
  }
  else {
    drupal_set_message(t('The content type <b>' . $bundle . '</b> already has the field <b>' . $field_name . '</b>.'), 'error', FALSE);
  }
}

/**
 * Menu callback; present a form for removing a field instance from a bundle.
 */
function ts_admin_field_delete_form($form, &$form_state, $instance) {
  module_load_include('inc', 'field_ui', 'field_ui.admin');
  $output = field_ui_field_delete_form($form, $form_state, $instance);

  $output['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/user-interface/ts/fields'),
  );

  return $output;
}

function ts_admin_field_delete_form_submit($form, &$form_state) {
  module_load_include('inc', 'field_ui', 'field_ui.admin');
  field_ui_field_delete_form_submit($form, $form_state);
  $form_state['redirect'] = 'admin/config/user-interface/ts/fields';
}

function ts_admin_get_terms($form, $form_state) {
  return $form['link_template_entities']['template_terms'];
}

function ts_admin_load_terms($selected) {
  $options = array();
  // load vocabulary to build the term tree.
  $vocab = taxonomy_vocabulary_machine_name_load($selected);
  // Get vocabulary ID.
  $vid = $vocab->vid;
  $terms = taxonomy_get_tree($vid);
  foreach ($terms as $term) {
    $options[] = $term->name;
  }
  return $options;
}
